//Cathy Tham, 1944919

package utilities;

public class MatrixMethod {
	public static int[][] duplicate(int[][] arr){
		/*Assume array is rectangle
		 *The second square brackets is multiply by the number of records in the first
		 *bracket at a position to be able to duplicate it. 
		 */
		int[][] duplicateArr=new int[arr.length][arr[0].length*2];
		
		for (int i=0; i<arr.length; i++){
			int duplicatesArrCount=0;
			for (int j=0; j<duplicateArr[i].length;j++) {
				duplicateArr[i][j]=arr[i][duplicatesArrCount];
				duplicatesArrCount++;
				//When the count is equal to the length of the arr, we go back to arr[i][0] to duplicate it.
				if(duplicatesArrCount==arr[0].length) {
					duplicatesArrCount=0;
				}
				
			}
		}
		return duplicateArr;
	}

	//Print the arr
	public static void main(String[] args) {
		int[][] arr = {{1,2,3,4,5,7,8},{4,5,6,0,7,7,8}};
		int[][] duplicateArr = duplicate(arr);
		
		for (int i = 0 ; i < duplicateArr.length ; i++) {
			for (int j = 0 ; j < duplicateArr[i].length ; j++) {
				System.out.print(duplicateArr[i][j]+" ");
			}
			System.out.println();
		}
		
	}
}



