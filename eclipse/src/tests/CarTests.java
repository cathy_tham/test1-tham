//Cathy Tham,1944919

package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import vehicules.Car;

class CarTests {
	//Testing Car constructor with a negative speed.
	@Test
	public void testConstructorNegativeSpeed() {
		try {
			Car car1=new Car(-2);
			fail("Did not catch the exception");
		}
		catch(IllegalArgumentException e) {
			
		}
		catch(Exception e) {
			fail ("Wrong exception");
			
		}
	}
	
	//Testing Car constructor with a positive speed.
	@Test
	public void testConstructorPositiveSpeed() {
		Car car1=new Car(2);
		int expectedSpeed=2;
		int expectedLocation=0;
		assertEquals(expectedSpeed, car1.getSpeed());
		assertEquals(expectedLocation, car1.getLocation());	
	}


	//Testing moveRight() 
	@Test
	public void moveRightTest() {
		Car car1 = new Car(3);
		car1.moveRight();
		assertEquals(3,car1.getLocation());
		assertEquals(3,car1.getSpeed());
	}
	
	//Testing moveLeft() 
	@Test
	public void moveLeftTest() {
		Car car1 = new Car(3);
		car1.moveLeft();
		assertEquals(-3,car1.getLocation());
		assertEquals(3, car1.getSpeed());
	}
	
	//Testing accelerate() 
	@Test
	void testAccelerate() {
		Car car1 = new Car(5);
		car1.accelerate();
		assertEquals(6,car1.getSpeed());
		
		//Test 2 accelerate
		Car car2 = new Car(6);
		car2.accelerate();
		car2.accelerate();
		assertEquals(8,car2.getSpeed());
	}
	
	//Testing decelerate() 
	@Test
	void testDecelerate() {
		Car car1 = new Car(5);
		car1.decelerate();
		assertEquals(4,car1.getSpeed());
		
		//Test 2 decelerate
		Car car2 = new Car(6);
		car2.decelerate();
		car2.decelerate();
		assertEquals(4,car2.getSpeed());
		
		//When car speed is 0, it should not change the speed
		Car car3 = new Car(0);
		car2.decelerate();
		assertEquals(0,car3.getSpeed());
	}

	
}
