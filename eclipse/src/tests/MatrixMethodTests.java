//Cathy Tham, 1944919

package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTests {

	@Test
	public void test1() {
		int[][] arr = {{1,2,3,4},{4,5,6,0}};
		int[][] expectedArr = {{1,2,3,4,1,2,3,4},{4,5,6,0,4,5,6,0}};
		int[][] duplicateArr = MatrixMethod.duplicate(arr);
		assertArrayEquals(expectedArr, duplicateArr);	
	}
	
	@Test
	public void test2() {
		int[][] arr = {{1,2},{4,5},{7,6}};
		int[][] expectedArr = {{1,2,1,2},{4,5,4,5},{7,6,7,6}};
		int[][] duplicateArr = MatrixMethod.duplicate(arr);
		assertArrayEquals(expectedArr, duplicateArr);	
	}


}
